package testproject.mapper;

import com.jkarkoszka.getjabbed.db.mapper.ObjectIdMapper;
import org.mapstruct.Mapper;
import testproject.db.BookDb;

@Mapper(uses = ObjectIdMapper.class)
public interface BookMapper {

  BookDb map(Book user);

  Book map(BookDb bookDb);
}
