package testproject.mapper;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Book {

  String id;

  String title;
}

