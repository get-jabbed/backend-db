package testproject.db;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarDbRepository extends MongoRepository<CarDb, ObjectId> {

}