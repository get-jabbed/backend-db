package testproject.db;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ItemDbRepository extends MongoRepository<ItemDb, ObjectId> {

}