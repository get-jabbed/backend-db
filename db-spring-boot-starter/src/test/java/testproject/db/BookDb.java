package testproject.db;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = BookDb.COLLECTION_NAME)
@Data
@Builder(toBuilder = true)
public class BookDb {

  public static final String COLLECTION_NAME = "books";

  @Id
  private ObjectId id;

  private String title;
}