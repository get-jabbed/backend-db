package testproject.db;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface PersonDbRepository extends ReactiveMongoRepository<PersonDb, ObjectId> {

}