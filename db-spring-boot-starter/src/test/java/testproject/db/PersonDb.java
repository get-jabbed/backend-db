package testproject.db;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = PersonDb.COLLECTION_NAME)
@Data
@Builder(toBuilder = true)
public class PersonDb {

  public static final String COLLECTION_NAME = "people";

  @Id
  private ObjectId id;

  private String name;

  private long age;
}