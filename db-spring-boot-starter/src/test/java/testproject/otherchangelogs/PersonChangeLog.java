package testproject.otherchangelogs;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.jkarkoszka.getjabbed.db.autoconfigure.DbChangeLogClass;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import testproject.db.PersonDb;
import testproject.db.PersonDbRepository;

@ChangeLog(order = "001-person-changelog")
@Component
public class PersonChangeLog implements DbChangeLogClass {

  public static ObjectId PERSON_ID = new ObjectId("507f191e810c19729de860ea");
  public static String PERSON_NAME = "Jakub";
  public static long PERSON_AGE = 33;

  @ChangeSet(author = "jkarkoszka", id = "PersonChangeLog#addFirstCollection", order = "001")
  public void addFirstCollection(MongoTemplate mongoTemplate) {
    if (!mongoTemplate.collectionExists(PersonDb.class)) {
      mongoTemplate.createCollection(PersonDb.class);
    }
  }

  @ChangeSet(author = "jkarkoszka", id = "PersonChangeLog#addSomeDataToCollection", order = "002")
  public void addSomeDataToCollection(PersonDbRepository personDbRepository) {
    var personDb = PersonDb.builder()
        .id(PERSON_ID)
        .name(PERSON_NAME)
        .age(PERSON_AGE)
        .build();
    personDbRepository.save(personDb).block();
  }
}
