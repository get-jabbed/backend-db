package testproject.otherchangelogs;

import com.jkarkoszka.getjabbed.db.autoconfigure.DbChangeLogClass;
import io.mongock.api.annotations.ChangeUnit;
import io.mongock.api.annotations.Execution;
import io.mongock.api.annotations.RollbackExecution;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;
import testproject.db.ItemDb;
import testproject.db.ItemDbRepository;

@ChangeUnit(id = "item-first-items", order = "2", author = "jkarkoszka")
@Component
@AllArgsConstructor
public class ItemFirstItemsChange implements DbChangeLogClass {

  public static ObjectId ITEM_ID = new ObjectId("507f1f77bcf86cd799439011");
  public static String ITEM_NAME = "Car";

  private final ItemDbRepository itemDbRepository;

  @Execution
  public void changeSet() {
    var item = ItemDb.builder()
        .id(ITEM_ID)
        .name(ITEM_NAME)
        .build();
    itemDbRepository.save(item);
  }

  @RollbackExecution
  public void rollback() {
  }
}
