package testproject.otherchangelogs;

import com.jkarkoszka.getjabbed.db.autoconfigure.DbChangeLogClass;
import io.mongock.api.annotations.ChangeUnit;
import io.mongock.api.annotations.Execution;
import io.mongock.api.annotations.RollbackExecution;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import testproject.db.ItemDb;

@ChangeUnit(id = "item-init", order = "1", author = "jkarkoszka")
@Component
@AllArgsConstructor
public class ItemInitChange implements DbChangeLogClass {

  private final MongoTemplate mongoTemplate;

  @Execution
  public void changeSet() {
    mongoTemplate.createCollection(ItemDb.class);
  }

  @RollbackExecution
  public void rollback() {
  }
}
