package testproject.changelogs;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import testproject.db.CarDb;
import testproject.db.CarDbRepository;

@ChangeLog(order = "000-car-changelog")
public class CarChangeLog {

  public static ObjectId CAR_ID = new ObjectId("507f1f77bcf86cd799439011");
  public static String CAR_NAME = "Mazda CX-30";

  @ChangeSet(author = "jkarkoszka", id = "CarChangeLog#addFirstCollection", order = "001")
  public void addFirstCollection(MongoTemplate mongoTemplate) {
    if (!mongoTemplate.collectionExists(CarDb.class)) {
      mongoTemplate.createCollection(CarDb.class);
    }
  }

  @ChangeSet(author = "jkarkoszka", id = "CarChangeLog#addSomeDataToCollection", order = "002")
  public void addSomeDataToCollection(CarDbRepository carDbRepository) {
    var carDb = CarDb.builder()
        .id(CAR_ID)
        .name(CAR_NAME)
        .build();
    carDbRepository.save(carDb);
  }
}
