package com.jkarkoszka.getjabbed.db.autoconfigure;

import static org.assertj.core.api.AssertionsForClassTypes.fail;

import com.jkarkoszka.getjabbed.db.mapper.RequiredDbCollectionDoesNotExistException;
import com.jkarkoszka.getjabbed.db.service.DbCollectionChecker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import testproject.TestApplication;
import testproject.db.BookDb;
import testproject.db.CarDb;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = TestApplication.class)
class DbUtilsAutoConfigurationTest {

  @Autowired
  DbCollectionChecker dbCollectionChecker;

  @Test
  public void shouldReturnNothingIfCollectionExists() {
    //when
    dbCollectionChecker.checkRequired(CarDb.class);
  }

  @Test
  public void shouldThrowExceptionIfCollectionExists() {
    //when
    try {
      dbCollectionChecker.checkRequired(BookDb.class);
      fail("expected exception");
    } catch (RequiredDbCollectionDoesNotExistException e) {
    }
  }
}