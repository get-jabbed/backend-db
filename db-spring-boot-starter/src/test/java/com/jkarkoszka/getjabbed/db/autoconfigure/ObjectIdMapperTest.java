package com.jkarkoszka.getjabbed.db.autoconfigure;

import static org.assertj.core.api.Assertions.assertThat;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import testproject.TestApplication;
import testproject.db.BookDb;
import testproject.mapper.Book;
import testproject.mapper.BookMapper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = TestApplication.class)
public class ObjectIdMapperTest {

  @Autowired
  BookMapper bookMapper;

  @Test
  public void shouldMapToBook() {
    var id = new ObjectId();
    var idAsString = id.toHexString();
    var title = "Book 1";
    var bookDb = BookDb.builder()
        .id(id)
        .title(title)
        .build();
    var expectedBook = Book.builder()
        .id(idAsString)
        .title(title)
        .build();

    //when
    var book = bookMapper.map(bookDb);

    //then
    assertThat(book).isEqualTo(expectedBook);
  }

  @Test
  public void shouldMapToBookDb() {
    var id = new ObjectId();
    var idAsString = id.toHexString();
    var title = "Book 1";
    var book = Book.builder()
        .id(idAsString)
        .title(title)
        .build();
    var expectedBookDb = BookDb.builder()
        .id(id)
        .title(title)
        .build();

    //when
    var bookDb = bookMapper.map(book);

    //then
    assertThat(bookDb).isEqualTo(expectedBookDb);
  }

  @Test
  public void shouldMapToBookIdIsNull() {
    var title = "Book 1";
    var bookDb = BookDb.builder()
        .title(title)
        .build();
    var expectedBook = Book.builder()
        .title(title)
        .build();

    //when
    var book = bookMapper.map(bookDb);

    //then
    assertThat(book).isEqualTo(expectedBook);
  }

  @Test
  public void shouldMapToBookDbIdIsNull() {
    var title = "Book 1";
    var book = Book.builder()
        .title(title)
        .build();
    var expectedBookDb = BookDb.builder()
        .title(title)
        .build();

    //when
    var bookDb = bookMapper.map(book);

    //then
    assertThat(bookDb).isEqualTo(expectedBookDb);
  }
}
