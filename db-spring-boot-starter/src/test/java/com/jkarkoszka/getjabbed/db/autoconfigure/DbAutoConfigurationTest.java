package com.jkarkoszka.getjabbed.db.autoconfigure;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static testproject.changelogs.CarChangeLog.CAR_ID;
import static testproject.changelogs.CarChangeLog.CAR_NAME;
import static testproject.otherchangelogs.ItemFirstItemsChange.ITEM_ID;
import static testproject.otherchangelogs.ItemFirstItemsChange.ITEM_NAME;
import static testproject.otherchangelogs.PersonChangeLog.PERSON_AGE;
import static testproject.otherchangelogs.PersonChangeLog.PERSON_ID;
import static testproject.otherchangelogs.PersonChangeLog.PERSON_NAME;

import com.jkarkoszka.getjabbed.db.service.DbCollectionChecker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import testproject.TestApplication;
import testproject.db.CarDb;
import testproject.db.CarDbRepository;
import testproject.db.ItemDb;
import testproject.db.ItemDbRepository;
import testproject.db.PersonDb;
import testproject.db.PersonDbRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = TestApplication.class)
public class DbAutoConfigurationTest {

  @Autowired
  MongoTemplate mongoTemplate;

  @Autowired
  CarDbRepository carDbRepository;

  @Autowired
  DbCollectionChecker dbCollectionChecker;

  @Autowired
  PersonDbRepository personDbRepository;

  @Autowired
  ItemDbRepository itemDbRepository;

  @Test
  public void shouldStartApplicationCorrectly() {
  }

  @Test
  public void carsCollectionShouldBeCreatedAndDataShouldBeAdded() {
    //then
    assertThat(mongoTemplate.collectionExists(CarDb.COLLECTION_NAME)).isTrue();
    var carDbOptional = carDbRepository.findById(CAR_ID);
    assertThat(carDbOptional.isPresent()).isTrue();
    var carDb = carDbOptional.get();
    assertThat(carDb.getName()).isEqualTo(CAR_NAME);
  }

  @Test
  public void peopleCollectionShouldBeCreatedAndDataShouldBeAdded() {
    //then
    assertThat(mongoTemplate.collectionExists(PersonDb.COLLECTION_NAME)).isTrue();
    var personDb = personDbRepository.findById(PERSON_ID).block();
    assertThat(personDb).isNotNull();
    assertThat(personDb.getId()).isEqualTo(PERSON_ID);
    assertThat(personDb.getName()).isEqualTo(PERSON_NAME);
    assertThat(personDb.getAge()).isEqualTo(PERSON_AGE);
  }

  @Test
  public void itemsCollectionShouldBeCreatedAndDataShouldBeAdded() {
    //then
    assertThat(mongoTemplate.collectionExists(ItemDb.COLLECTION_NAME)).isTrue();
    var itemDbOptional = itemDbRepository.findById(ITEM_ID);
    assertThat(itemDbOptional.isPresent()).isTrue();
    var itemDb = itemDbOptional.get();
    assertThat(itemDb.getName()).isEqualTo(ITEM_NAME);
  }
}
