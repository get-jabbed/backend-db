package com.jkarkoszka.getjabbed.db.autoconfigure;

import com.jkarkoszka.getjabbed.db.mapper.ObjectIdMapper;
import com.jkarkoszka.getjabbed.db.service.DbCollectionChecker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableConfigurationProperties(DbProperties.class)
@Slf4j
@AllArgsConstructor
class DbUtilsAutoConfiguration {

  @Bean
  public DbCollectionChecker dbCollectionChecker(MongoTemplate mongoTemplate) {
    return new DbCollectionChecker(mongoTemplate);
  }

  @Bean
  public ObjectIdMapper objectIdMapper() {
    return new ObjectIdMapper();
  }
}
