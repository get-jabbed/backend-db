package com.jkarkoszka.getjabbed.db.autoconfigure;

import static com.mongodb.WriteConcern.ACKNOWLEDGED;

import com.mongodb.ReadConcern;
import io.mongock.driver.mongodb.springdata.v3.SpringDataMongoV3Driver;
import io.mongock.runner.springboot.MongockSpringboot;
import io.mongock.runner.springboot.base.MongockInitializingBeanRunner;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@PropertySource("classpath:db.properties")
@Slf4j
@AllArgsConstructor
class DbAutoConfiguration {

  private final DbProperties dbProperties;
  private List<DbChangeLogClass> dbChangeLogClasses;

  /**
   * Method which configures Mongock, tool for db migrations.
   *
   * @param mongoTemplate MongoTemplate bean
   * @param springContext ApplicationContext bean
   * @return bean MongockInitializingBeanRunner
   */
  @Bean
  public MongockInitializingBeanRunner mongockInitializingBeanRunner(
      ApplicationContext springContext,
      MongoTemplate mongoTemplate) {
    var springDataMongoV3Driver = SpringDataMongoV3Driver.withDefaultLock(mongoTemplate);
    springDataMongoV3Driver.setReadConcern(ReadConcern.LOCAL);
    springDataMongoV3Driver.setWriteConcern(ACKNOWLEDGED.withJournal(false));
    springDataMongoV3Driver.setReadConcern(ReadConcern.LOCAL);
    springDataMongoV3Driver.setWriteConcern(ACKNOWLEDGED.withJournal(false));
    var mongockSpringBuilder = MongockSpringboot.builder()
        .setDriver(springDataMongoV3Driver)
        .addMigrationClasses(getChangeLogClasses())
        .setSpringContext(springContext);
    if (StringUtils.isNotBlank(dbProperties.getChangeLogsScanPackage())) {
      mongockSpringBuilder.addMigrationScanPackage(dbProperties.getChangeLogsScanPackage());
    }
    return mongockSpringBuilder.buildInitializingBeanRunner();
  }

  private List<Class<?>> getChangeLogClasses() {
    if (Objects.isNull(dbChangeLogClasses)) {
      return List.of();
    }
    return dbChangeLogClasses.stream()
        .map(DbChangeLogClass::getClass)
        .collect(Collectors.toList());
  }
}
