package com.jkarkoszka.getjabbed.db.mapper;

import java.util.Objects;
import org.bson.types.ObjectId;

/**
 * Mapper class to map ObjectId to String and String to ObjectId.
 */
public class ObjectIdMapper {

  /**
   * Method to map value from ObjectId to String.
   *
   * @param value value
   * @return value as string
   */
  public String map(ObjectId value) {
    if (Objects.isNull(value)) {
      return null;
    }
    return value.toHexString();
  }

  /**
   * Method to map value from String to ObjectId.
   *
   * @param value value
   * @return value as ObjectId
   */
  public ObjectId map(String value) {
    if (Objects.isNull(value)) {
      return null;
    }
    return new ObjectId(value);
  }
}
