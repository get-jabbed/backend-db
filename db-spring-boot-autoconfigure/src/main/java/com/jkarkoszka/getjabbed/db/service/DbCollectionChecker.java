package com.jkarkoszka.getjabbed.db.service;

import com.jkarkoszka.getjabbed.db.mapper.RequiredDbCollectionDoesNotExistException;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Class with method to check if required collection exists in the database.
 */
@AllArgsConstructor
public class DbCollectionChecker {

  private final MongoTemplate mongoTemplate;

  /**
   * Method to check if required collection exists in the database.
   *
   * @param collectionClass collection class
   * @param <T>             any db class
   */
  public <T> void checkRequired(Class<T> collectionClass) {
    if (!mongoTemplate.collectionExists(collectionClass)) {
      throw new RequiredDbCollectionDoesNotExistException(collectionClass);
    }
  }
}
