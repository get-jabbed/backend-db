package com.jkarkoszka.getjabbed.db.mapper;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Exception thrown when required collection in the DB does not exist.
 */
public class RequiredDbCollectionDoesNotExistException extends RuntimeException {

  /**
   * Constructor for RequiredDbCollectionDoesNotExistException.
   *
   * @param collectionClass need check class
   * @param <T>             class type
   */
  public <T> RequiredDbCollectionDoesNotExistException(Class<T> collectionClass) {
    super("Required collection " + collectionClass.getAnnotation(Document.class).collection()
        + " does not exist.");
  }
}
