package com.jkarkoszka.getjabbed.db.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class with the backend-db configuration.
 */
@ConfigurationProperties(prefix = "getjabbed.db")
@Data
public class DbProperties {

  private String changeLogsScanPackage;

}
